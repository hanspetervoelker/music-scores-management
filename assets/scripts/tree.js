

Vue.component('work-leave', {
  name: 'workLeave',
  props: {
    work: Object,
  },
  template: `<li class="leave"><span class="worknode treetext"> {{ work.title }}</span></li>`,

})

Vue.component('composer-node', {
  name: 'composerNode',
  props: {
    composer: Object,
  },
  template: `<li class="node">
              <span v-if="hasComposers" v-on:click="toggleWork" class="caret">{{worksExpanded ? '&#9660;' : '&#9658;'}}</span>
              <span v-else class="hidden-caret">&#9658;</span>
              <span class="treetext">{{ composer.lastname }}, {{ composer.firstname }} ({{ composer.workCount }})</span>
              <ul v-show="worksExpanded" class="nodechilds works">
                <work-leave v-for="work in composer.works" v-bind:work="work" v-bind:key="work.id"></work-leave>
              </ul>
            </li>`,
  data() {
    return {
      worksExpanded: false
    }
  },
  methods: {
    toggleWork() {
      this.worksExpanded = !this.worksExpanded;
    }
  },
  computed: {
    hasComposers() {
      return this.composer.workCount>0;
    }
  },
})

Vue.component('epoche-node', {
  name: 'epocheNode',
  props: {
    epoche: Object,
  },
  template: `<li class="node">
              <span v-if="hasWorks" class="caret" v-on:click="toggleComp">{{composerExpanded ? '&#9660;' : '&#9658;'}}</span>
              <span class="treetext">{{ epoche.name }} ({{ epoche.composerCount }})</span>
              <ul v-show="composerExpanded" class="nodechilds">
                <composer-node v-for="composer in epoche.composers" v-bind:composer="composer" v-bind:key="composer.id"></composer-node>
              </ul>
            </li>`,
  data() {
    return {
      composerExpanded: false
    }
  },
  methods: {
    toggleComp() {
      this.composerExpanded = !this.composerExpanded;
    }
  },
  computed: {
    hasWorks() {
      return this.epoche.composerCount>0;
    }
  },
})

Vue.component('epoche-tree', {
  name: 'epocheTree',
  props: {
    epoches: Array,
  },
  template: `<ul v-if="hasChildren" class="epochetree">
              <epoche-node v-for="epoche in epoches" v-bind:epoche="epoche" v-bind:key="epoche.id"></epoche-node>
             </ul>`,
  computed: {
    hasChildren() {
      return this.epoches.length>0;
    }
  },
})


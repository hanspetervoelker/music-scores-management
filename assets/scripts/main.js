
// Ajax for getting Epoch-Info
function getEpocheInfos(id, cb) {
  getAjax("http://localhost:8088/epoches", cb, id);
}

function getTreeInfos(cb) {
  getAjax("http://localhost:8088/epoches", cb, null);
}

function getAjax(endpoint, cb, id) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     cb(this);
    }
  };
  url = endpoint;
  if(id) {
    url = url+"/"+id;
  }
  xhttp.open("GET", url, true);
  xhttp.send();
}

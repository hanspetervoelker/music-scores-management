CREATE (agu:Person{name: "aguado", lastname: "Aguado", firstname: "Dionisio", yob: 1784, yod: 1849, nationality: "spanish"})
CREATE (sor:Person{name: "sor", lastname: "Sor", firstname: "Fernando", yob: 1778, yod: 1839, nationality: "spanish"})
CREATE (car:Person{name: "carulli", lastname: "Carulli", firstname: "Ferdinando", yob: 1770, yod: 1841, nationality: "italian"})
CREATE (tar:Person{name: "tarrega", lastname: "Tarrega", firstname: "Francisco", yob: 1852, yod: 1909, nationality: "spanish"})
CREATE (git:Persontype{name: "guitarist"})
CREATE (com:Persontype{name: "composer"})
CREATE (tea:Persontype{name: "guitar teacher"})
CREATE (arr:Persontype{name: "arranger"})
CREATE (aut:Persontype{name: "author"})
CREATE (bar:Epoche{name: "Baroque", from: 1600, to:1750})
CREATE (cla:Epoche{name: "Classical Period", from: 1750, to:1840})
CREATE (rom:Epoche{name: "Romantic", from: 1840, to:1920})
CREATE (ren:Epoche{name: "Renaissance", from: 1400, to:1600})
CREATE (mod:Epoche{name: "Modern", from: 1920})
CREATE (std:Tuning{name: "Standard", tuning: "EADgbe"})
CREATE (drd:Tuning{name: "Droped D", tuning: "DADgbe"})
CREATE (op7:Work{title: "8 Easy Waltzes", subtitle: "Opus 7", type: "collection"})
CREATE (sol:Arrangement{name: "Solo Guitar"})
CREATE (due:Arrangement{name: "Guitar Duet"})
CREATE (pia:Arrangement{name: "Solo Piano"})
CREATE (cmj:Key {name: "C-Major"})
CREATE (gmj:Key {name: "G-Major"})
CREATE (dmj:Key {name: "D-Major"})
CREATE (vea:Difficulty {name: "very easy"})
CREATE (eas:Difficulty {name: "easy"})
CREATE (med:Difficulty {name: "medium"})
CREATE (dif:Difficulty {name: "difficult"})
CREATE (vdi:Difficulty {name: "very difficult"})
CREATE (ccl:Category {name: "classical"})
CREATE (cpo:Category {name: "pop"})
CREATE (cbl:Category {name: "blues"})
CREATE (cja:Category {name: "jazz"})
CREATE (wn1:Piece{name: "Waltz No.1", type: "piece", thumbnail: "01Waltz01.png"})
CREATE (wn2:Piece{name: "Waltz No.2", type: "piece", thumbnail: "02Waltz02.png"})
CREATE (pdf:Media {filename: "WaltzesOp7Aguado.pdf", mime: "application/pdf"})
CREATE (mp1:Media {filename: "01Waltz_1.mp3", mime: "audio/mpeg"})
CREATE (mp2:Media {filename: "02Waltz_2.mp3", mime: "audio/mpeg"})

CREATE
  (agu)-[:BELONGS_TO]->(cla),
  (sor)-[:BELONGS_TO]->(cla),
  (car)-[:BELONGS_TO]->(cla),
  (tar)-[:BELONGS_TO]->(rom),
  (agu)-[:CREATED]->(op7),
  (agu)-[:CREATED]->(wn1),
  (agu)-[:CREATED]->(wn2),
  (agu)-[:IS_A]->(git),
  (agu)-[:IS_A]->(com),
  (agu)-[:IS_A]->(tea),
  (agu)-[:IS_A]->(aut),
  (sor)-[:IS_A]->(git),
  (sor)-[:IS_A]->(com),
  (sor)-[:IS_A]->(tea),
  (sor)-[:IS_A]->(aut),
  (car)-[:IS_A]->(git),
  (car)-[:IS_A]->(com),
  (car)-[:IS_A]->(tea),
  (car)-[:IS_A]->(aut),
  (tar)-[:IS_A]->(git),
  (tar)-[:IS_A]->(com),
  (tar)-[:IS_A]->(tea),
  (agu)-[:IS_A]->(aut),
  (wn1)-[:PLAYED_IN]->(std),
  (wn1)-[:PART_OF]->(op7),
  (wn1)-[:ARRANGED_FOR]->(sol),
  (wn1)-[:CREATED_BY {creation_type: "composed"}]->(agu),
  (op7)-[:CREATED_BY {creation_type: "composed"}]->(agu),
  (wn1)-[:IN_KEY_OF]->(gmj),
  (wn1)-[:IS_GRADED_AS]->(eas),
  (wn1)-[:IS_CATEGORIZED_AS]->(ccl),
  (wn1)-[:AVAILABLE_AS{type: "notation"}]->(pdf),
  (wn1)-[:AVAILABLE_AS{type: "audio"}]->(mp1),
  (wn2)-[:PLAYED_IN]->(drd),
  (wn2)-[:PART_OF]->(op7),
  (wn2)-[:ARRANGED_FOR]->(sol),
  (wn2)-[:CREATED_BY {creation_type: "composed"}]->(agu),
  (wn2)-[:IN_KEY_OF]->(dmj),
  (wn2)-[:IS_GRADED_AS]->(eas),
  (wn2)-[:IS_CATEGORIZED_AS]->(ccl),
  (wn2)-[:AVAILABLE_AS{type: "notation"}]->(pdf),
  (wn2)-[:AVAILABLE_AS{type: "audio"}]->(mp2),
  (op7)-[:AVAILABLE_AS{type: "notation"}]->(pdf),
  (op7)-[:CREATED_BY {creation_type: "written"}]->(agu)

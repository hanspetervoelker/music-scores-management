package models

const (
	// GetTreeData is the match-statement for getting epoche, composer, works infos
	GetTreeData = `MATCH (e:Epoche) <-[rc:BELONGS_TO]-(p:Person)-[rp:IS_A]->(pt:Persontype{name:"composer"})
	OPTIONAL MATCH (p:Person)<-[rw:CREATED_BY]-(w:Work)
	OPTIONAL MATCH (w)<-[rt:PART_OF]-(t:Piece)
	WITH e, p, w, t
	ORDER BY e.from, p.lastname, w.title, t.name
	WITH e, p, w, {work: w.title, id: id(w), pieces_count: count(distinct t)} as p_works
	WITH e, w, {composer: {id: id(p), lastname: p.lastname, firstname: p.firstname}, works_count: count(w) , works: CASE WHEN w IS NOT NULL THEN  collect(p_works) ELSE NULL END} as composers
	RETURN {epoche: e.name, id: id(e), composer_count: count(composers), composers: collect(composers)} as epoche`
	// GetEpocheInfo is the statement for getting Details about an epoche and its composers
	GetEpocheInfo = `MATCH (e:Epoche)<-[BELONGS_TO]-(p:Person)-[:IS_A]->(:Persontype{name: "composer"})
	WHERE ID(e) = %s
	OPTIONAL MATCH (p)-[CREATED]->(w:Work)
	WITH e, p, count(w) as cw
	ORDER BY p.lastname, p.firstname
	WITH e, {firstname: p.firstname, lastname: p.lastname, nationality: p.nationality, yob: p.yob, yod: p.yod, id: id(p), workcount: cw} as comp
	RETURN {id: id(e), name: e.name, from: e.from, to: e.to, composers: collect(comp)} as info`
	// CreateNewComposer is the statement for creating a new composer (creates rel to the epoche and persontype)
	CreateNewComposer = `MATCH (e:Epoche), (t:Persontype) WHERE ID(e) = %s AND t.name = "composer"
	CREATE
		(n:Person{name: "%s", lastname: "%s", firstname: "%s", yob: %s, yod: %s, nationality: "%s"}),
		(n)-[:BELONGS_TO]->(e),
		(n)-[:IS_A]->(t)
	RETURN {firstname: n.firstname, lastname: n.lastname, nationality: n.nationality, yob: n.yob, yod: n.yod, id: id(n), workcount: 0} as comp`
	// DeleteNode is the statement to delete a person node and its relations by id
	DeleteNode = `MATCH (n:Person)
	WHERE ID(n) = %s
	DETACH DELETE n`
)

package models

import (
	"fmt"

	bolt "github.com/johnnadratowski/golang-neo4j-bolt-driver"
)

// WorkNode describes a Work Node in an Epoche Tree
type WorkNode struct {
	Title      string `json:"title"`
	PieceCount int64  `json:"pieceCount"`
	ID         int64  `json:"id"`
}

// ComposerNode describes a ComposerNode in an Epoche Tree
type ComposerNode struct {
	Lastname  string     `json:"lastname"`
	Firstname string     `json:"firstname"`
	ID        int64      `json:"id"`
	WorkCount int64      `json:"workCount"`
	Works     []WorkNode `json:"works"`
}

// EpocheNode describes an Epoche Node in an Epoche Tree
type EpocheNode struct {
	Name          string         `json:"name"`
	ID            int64          `json:"id"`
	ComposerCount int64          `json:"composerCount"`
	Composers     []ComposerNode `json:"composers"`
}

// EpocheTree is a tree struct composed of epoches, composers and works
type EpocheTree []EpocheNode

// GetEpocheTree returns a nested struct for epoches-composers-works. It uses the GetTreeData-Statement to get the data from the db
func GetEpocheTree() EpocheTree {
	var et EpocheTree
	d := GetData(GetTreeData) // Get Data from db
	// Iterate over Epoch-Records
	for _, e := range d {
		epoches := e[0].(map[string]interface{})
		// create EpocheNode
		en := EpocheNode{
			Name:          epoches["epoche"].(string),
			ID:            epoches["id"].(int64),
			ComposerCount: epoches["composer_count"].(int64),
		}
		// Iterate over Composer-Array for that epoche
		for _, c := range epoches["composers"].([]interface{}) {
			cc := c.(map[string]interface{})
			comp := cc["composer"].(map[string]interface{})
			// create ComposerNode
			cn := ComposerNode{
				Lastname:  comp["lastname"].(string),
				Firstname: comp["firstname"].(string),
				ID:        comp["id"].(int64),
				WorkCount: cc["works_count"].(int64),
			}
			if cn.WorkCount > 0 { // if there are works for that composer
				// iterate over Works for that composer
				for _, w := range cc["works"].([]interface{}) {
					// create WorkNode
					wn := WorkNode{
						ID:         w.(map[string]interface{})["id"].(int64),
						PieceCount: w.(map[string]interface{})["pieces_count"].(int64),
						Title:      w.(map[string]interface{})["work"].(string),
					}
					cn.Works = append(cn.Works, wn) // add work to worklist of that composer
				}
			}
			en.Composers = append(en.Composers, cn) // add composer to composerlist of that epoche
		}
		et = append(et, en) // add epeoche to epochelist
	}
	return et
}

// GetEpocheInfos returns details of a epoch incl. composers
func GetEpocheInfos(id string) EpocheInfo {
	var ei EpocheInfo
	stmt := fmt.Sprintf(GetEpocheInfo, id)
	d := GetData(stmt)[0][0] // Get Data from db
	dd := d.(map[string]interface{})
	ei = EpocheInfo{
		ID:   dd["id"].(int64),
		Name: dd["name"].(string),
		From: dd["from"].(int64),
		To:   dd["to"].(int64),
	}
	comp := dd["composers"].([]interface{})
	for _, c := range comp {
		cc := c.(map[string]interface{})
		var comp = ComposerInfo{
			Firstname:   cc["firstname"].(string),
			Lastname:    cc["lastname"].(string),
			ID:          cc["id"].(int64),
			Nationality: cc["nationality"].(string),
			YoB:         cc["yob"].(int64),
			YoD:         cc["yod"].(int64),
			WorkCount:   cc["workcount"].(int64),
		}
		ei.Composers = append(ei.Composers, comp)
	}
	return ei
}

// ComposerInfo describes attributes of a composer for use in json endpoint
type ComposerInfo struct {
	ID          int64  `json:"id"`
	Firstname   string `json:"firstname"`
	Lastname    string `json:"lastname"`
	Nationality string `json:"nationality"`
	YoB         int64  `json:"yearOfBirth"`
	YoD         int64  `json:"yearOfDeath"`
	WorkCount   int64  `json:"workCount"`
}

// EpocheInfo describes attributes of an epoch for use in json endpoint
type EpocheInfo struct {
	ID        int64          `json:"id"`
	Name      string         `json:"name"`
	From      int64          `json:"from"`
	To        int64          `json:"to"`
	Composers []ComposerInfo `json:"composers"`
}

// GetData is generic function for querying database
func GetData(stmt string) [][]interface{} {
	driver := bolt.NewDriver()
	conn, _ := driver.OpenNeo("bolt://hpv:hpv@localhost:7687")
	defer conn.Close()
	data, _, _, _ := conn.QueryNeoAll(stmt, nil)
	return data
}

// DeleteByID delets a node with the given od and its relations
func DeleteByID(id string) int64 {
	stmt := fmt.Sprintf(DeleteNode, id)
	driver := bolt.NewDriver()
	conn, _ := driver.OpenNeo("bolt://hpv:hpv@localhost:7687")
	defer conn.Close()
	result, _ := conn.ExecNeo(stmt, nil)
	numResult, _ := result.RowsAffected()
	return numResult
}

package models

import (
	"fmt"
)

func ExampleGetEpocheTree() {
	et := GetEpocheInfos("34")
	fmt.Println(et)
	// output:
	// [[Aguado Dionisio] [Sor Fernando] [Carulli Ferdinando]]
	// Dionisio
	// map[result_available_after:0 fields:[n.lastname n.firstname]]
	// n.firstname
}

MATCH (e:Epoche) -[rc:BELONGS_TO]-(p:Person)-[rp:IS_A]-(pt:Persontype{name:"composer"})
OPTIONAL MATCH (p:Person)-[rw:CREATED_BY]-(w:Work)-[rt:PART_OF]-(t:Piece)
WITH e, p, w, t
ORDER BY e.from, p.lastname, w.title, t.name
WITH e, p, w, {piece: t.name} as w_piece
WITH e, p, {work: w.title, pieces: collect(w_piece)} as p_works
RETURN e.name as epoche, p.name as composer, collect(p_works) as works

//MATCH (p) WHERE id(p) = 11
//OPTIONAL MATCH (p) -[:car]- (c)
//OPTIONAL MATCH (p) -[:driver]- (u)
//RETURN {
//  _id: id(p), name: p.name, type: p.type,
//  cars: CASE WHEN c IS NOT NULL THEN collect({_id: id(c), name: c.name}) ELSE NULL END,
//  drivers: CASE WHEN u IS NOT NULL THEN collect({_id: id(u), name: u.email}) ELSE NULL END
//} AS place

MATCH (e:Epoche) -[rc:BELONGS_TO]-(p:Person)-[rp:IS_A]-(pt:Persontype{name:"composer"})
OPTIONAL MATCH (p:Person)-[rw:CREATED_BY]-(w:Work)
OPTIONAL MATCH (w)-[rt:PART_OF]-(t:Piece)
WITH e, p, w, t
ORDER BY e.from, p.lastname, w.title, t.name
WITH e, p, w, {work: w.title, id: id(w), pieces_count: count(t)} as p_works
WITH e, w, {composer: {id: id(p), lastname: p.lastname, firstname: p.firstname}, works_count: count(w) , works: CASE WHEN w IS NOT NULL THEN  collect(p_works) ELSE NULL END} as composers
RETURN {epoche: e.name, id: id(e), composer_count: count(composers), composers: collect(composers)} as epoche

MATCH (e:Epoche)<-[BELONGS_TO]-(p:Person)-[:IS_A]->(:Persontype{name: "composer"})
WHERE ID(e) = %s
OPTIONAL MATCH (p)-[CREATED]->(w:Work)
WITH e, p, count(w) as cw
ORDER BY p.lastname, p.firstname
WITH e, {firstname: p.firstname, lastname: p.lastname, nationality: p.nationality, yob: p.yob, yod: p.yod, id: id(p), workcount: cw} as comp
RETURN {id: id(e), name: e.name, from: e.from, to: e.to, composers: collect(comp)} as info

MATCH (e:Epoche), (t:Persontype) WHERE ID(e) = 0 AND t.name = "composer"
CREATE
  (n:Person{name: "test", lastname: "test", firstname: "erster", yob: 1780, yod: 1840, nationality: "german"}),
  (n)-[:BELONGS_TO]->(e),
  (n)-[:IS_A]->(t)

MATCH (n:Person)
WHERE ID(n) = {%s}
DETACH DELETE n
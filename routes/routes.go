package routes

import (
	"encoding/json"
	"html/template"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/music-scores-management/models"
)

// EpochesHandler is the http-handler for route /epoches GET and returns an array of epoches incl. composers and works (for treeview)
func EpochesHandler(w http.ResponseWriter, r *http.Request) {
	e := models.GetEpocheTree()
	json, _ := json.Marshal(e)
	w.Write(json)
}

// Roothandler is the http-handler for route "/"
func Roothandler(w http.ResponseWriter, r *http.Request) {
	data := struct {
		Title string
		Tree  models.EpocheTree
		Cache struct {
			Epoch    string
			Composer string
			Work     string
			Action   string
			View     string
		}
	}{
		Title: "Music Score Mangement",
	}

	t, _ := template.ParseFiles("./templates/index.html")
	t.Execute(w, data)
}

// EpocheInfoHandler is an endpoint for GET-Request
func EpocheInfoHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	e := models.GetEpocheInfos(id)
	json, _ := json.Marshal(e)
	w.Write(json)

}

// DeleteNodeHandler is an endpoint for DELETE-Request
func DeleteNodeHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	res := models.DeleteByID(id)
	json, _ := json.Marshal(res)
	w.Write(json)
}

//r.HandleFunc("/books/{title}", ReadBook).Methods("GET")

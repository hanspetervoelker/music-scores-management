package main

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/music-scores-management/routes"
)

func main() {
	router := mux.NewRouter()

	router.HandleFunc("/", routes.Roothandler)
	router.HandleFunc("/epoches", routes.EpochesHandler)
	router.HandleFunc("/epoches/{id}", routes.EpocheInfoHandler)
	router.HandleFunc("/composers/{id}", routes.DeleteNodeHandler).Methods("DELETE")

	router.PathPrefix("/static/").Handler(
		http.StripPrefix("/static/",
			http.FileServer(http.Dir("./assets/"))))

	http.ListenAndServe(":8088", router)
}
